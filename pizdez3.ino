#include <Wire.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

#define HEART_RATE_BLUETOOTH
#define MATRIX_ACCEL
#define GPS

#ifdef MATRIX_ACCEL
const short MPU = 0x68;
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;

#define IIC_SCL  A3
#define IIC_SDA  A2

static const int RXPin = 4, TXPin = 3;
static const uint32_t GPSBaud = 9600;

TinyGPSPlus gps;

SoftwareSerial ss(RXPin, TXPin);

unsigned char data_line = 0;
unsigned char delay_count = 0;
unsigned char data_display1 = 0;
unsigned char data_display2 = 0;
unsigned char data_display3 = 0;
unsigned char data_display4 = 0;
unsigned char data_display5 = 0;
unsigned char data_display6 = 0;
unsigned char data_display7 = 0;
unsigned char data_display8 = 0;
unsigned char data_display9 = 0;
unsigned char data_display10 = 0;
unsigned char data_display11 = 0;
unsigned char data_display12 = 0;
unsigned char data_display13 = 0;
unsigned char data_display14 = 0;
unsigned char data_display15 = 0;
unsigned char data_display16 = 0;

void IIC_start();

void IIC_send(unsigned char send_data);

void IIC_end();

unsigned char table[16][16] = {{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42, 0x81},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00},
                               {0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x83, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18},
                               {0x00, 0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24},
                               {0x00, 0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42},
                               {0x00, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42, 0x81},
                               {0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00, 0x18, 0x24, 0x42, 0x81, 0x00}};
unsigned char table_reverse[16][16] = {{0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00},
                                       {0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00, 0x00},
                                       {0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00, 0x00},
                                       {0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x00},
                                       {0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00, 0x81, 0x42, 0x24, 0x18, 0x00}};

void gps_setup() 
{
    Serial.begin(9600);
    ss.begin(GPSBaud);
}

void gps_loop() 
{
    while (ss.available() > 0) 
    {
        gps.encode(ss.read());
        if (gps.location.isUpdated()) 
        {
            Serial.print("Latitude= ");
            Serial.print(gps.location.lat(), 6);
            Serial.print(" Longitude= ");
            Serial.println(gps.location.lng(), 6);
        }
    }
}

void matrix_setup() 
{
    pinMode(IIC_SCL, OUTPUT);
    pinMode(IIC_SDA, OUTPUT);
    digitalWrite(IIC_SCL, LOW);
    digitalWrite(IIC_SDA, LOW);
}

void matrix_reverse_loop() 
{
    IIC_start();
    IIC_send(0x40);
    IIC_end();
    IIC_start();
    IIC_send(0xc0);
    for (char i = 0; i < 16; i++) 
    {
        IIC_send(table_reverse[data_line][i]);
    }
    if (++delay_count >= 10) 
    {
        delay_count = 0;
        data_line++;
        if (data_line >= 4) 
        {
            data_line = 0;
        }
    }

    IIC_end();
    IIC_start();
    IIC_send(0x8A);
    IIC_end();
}

void matrix_loop() 
{
    IIC_start();
    IIC_send(0x40);
    IIC_end();
    IIC_start();
    IIC_send(0xc0);
    for (char i = 0; i < 16; i++) 
    {
        IIC_send(table[data_line][i]);
    }
    if (++delay_count >= 10) 
    {
        delay_count = 0;
        data_line++;
        if (data_line >= 4) 
        {
            data_line = 0;
        }
    }

    IIC_end();
    IIC_start();
    IIC_send(0x8A);
    IIC_end();
}

void IIC_start()
{
    digitalWrite(IIC_SCL, LOW);
    delayMicroseconds(3);
    digitalWrite(IIC_SDA, HIGH);
    delayMicroseconds(3);
    digitalWrite(IIC_SCL, HIGH);
    delayMicroseconds(3);
    digitalWrite(IIC_SDA, LOW);
    delayMicroseconds(3);
}

void IIC_send(unsigned char send_data) 
{
    for (char i = 0; i < 8; i++)
    {
        digitalWrite(IIC_SCL, LOW);
        delayMicroseconds(3);
        if (send_data & 0x01) 
        {
            digitalWrite(IIC_SDA, HIGH);
        } 
        else 
        {
            digitalWrite(IIC_SDA, LOW);
        }
        delayMicroseconds(3);
        digitalWrite(IIC_SCL, HIGH);
        delayMicroseconds(3);
        send_data = send_data >> 1;
    }
}

void IIC_end() 
{
    digitalWrite(IIC_SCL, LOW);
    delayMicroseconds(3);
    digitalWrite(IIC_SDA, LOW);
    delayMicroseconds(3);
    digitalWrite(IIC_SCL, HIGH);
    delayMicroseconds(3);
    digitalWrite(IIC_SDA, HIGH);
    delayMicroseconds(3);
}

void accelSetup()
{
    Wire.begin();
    Wire.beginTransmission(MPU);
    Wire.write(0x6B);
    Wire.write(0);
    Wire.endTransmission(true);
    Serial.begin(9600);
    matrix_setup();
}

void accelLoop()
{
    Wire.beginTransmission(MPU);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 12, true);
    AcX = Wire.read() << 8 | Wire.read();
    AcY = Wire.read() << 8 | Wire.read();
    AcZ = Wire.read() << 8 | Wire.read();
    GyX = Wire.read() << 8 | Wire.read();
    GyY = Wire.read() << 8 | Wire.read();
    GyZ = Wire.read() << 8 | Wire.read();
    if (AcY > 8000 && AcZ < 12000) 
    {
        matrix_reverse_loop();
        Serial.print("Right");
    } 
    else if (AcX < 2000 && AcY < -7500) 
    {
        matrix_loop();
        Serial.print("Left");
    } 
    else if (AcX > 8000 && AcY < 1000) 
    {
        Serial.print("Side2");
        matrix_loop();
    } 
    else if (AcX < -8000 && AcY < 1000) 
    {
        Serial.print("Side3");
        matrix_loop();
    }
    Serial.println(" ");
}
#endif // MATRIX_ACCEL

#ifdef HEART_RATE_BLUETOOTH
SoftwareSerial Genotronex(10, 11);
int ledpin = 12;
int BluetoothData;

int pulsePin = 0;
int blinkPin = 13;
int fadePin = 5;
int fadeRate = 0;

volatile int BPM;
volatile int Signal;
volatile int IBI = 600;
volatile boolean Pulse = false;
volatile boolean QS = false;

static boolean serialVisual = true;


volatile int rate[10];
volatile unsigned long sampleCounter = 0;
volatile unsigned long lastBeatTime = 0;
volatile int P = 512;
volatile int T = 512;
volatile int thresh = 525;
volatile int amp = 100;
volatile boolean firstBeat = true;
volatile boolean secondBeat = false;

void interruptSetup() 
{
    TCCR2A = 0x02;
    TCCR2B = 0x06;
    OCR2A = 0X7C;
    TIMSK2 = 0x02;
    sei();
}

ISR(TIMER2_COMPA_vect) 
{
    cli();
    Signal = analogRead(pulsePin);
    sampleCounter += 2;
    int N = sampleCounter - lastBeatTime;


    if (Signal < thresh && N > (IBI / 5) * 3) 
    {
        if (Signal < T) 
        {
            T = Signal;
        }
    }

    if (Signal > thresh && Signal > P) 
    {
        P = Signal;
    }


    if (N > 250) 
    {
        if ((Signal > thresh) && (Pulse == false) && (N > (IBI / 5) * 3)) 
        {
            Pulse = true;
            IBI = sampleCounter - lastBeatTime;
            lastBeatTime = sampleCounter;

            if (secondBeat) 
            {
                secondBeat = false;
                for (int i = 0; i <= 9; i++)
                {
                    rate[i] = IBI;
                }
            }

            if (firstBeat) 
            {
                firstBeat = false;
                secondBeat = true;
                sei();
                return;
            }


            word runningTotal = 0;
            for (int i = 0; i <= 8; i++)
            {
                rate[i] = rate[i + 1];
                runningTotal += rate[i];
            }

            rate[9] = IBI;
            runningTotal += rate[9];
            runningTotal /= 10;
            BPM = 60000 / runningTotal;
            QS = true;
        }
    }

    if (Signal < thresh && Pulse == true) 
    {

        Pulse = false;
        amp = P - T;
        thresh = amp / 2 + T;
        P = thresh;
        T = thresh;
    }

    if (N > 2500) 
    {
        thresh = 512;
        P = 512;
        T = 512;
        lastBeatTime = sampleCounter;
        firstBeat = true;
        secondBeat = false;
    }

    sei();
}

void serialOutput()
{
    if (serialVisual == true) 
    {
        arduinoSerialMonitorVisual('-', Signal);
    } 
    else 
    {
        sendDataToSerial('S', Signal);
    }
}

void serialOutputWhenBeatHappens() 
{
    if (serialVisual == true) 
    {
        Serial.print("*** Heart-Beat Happened *** ");
        Serial.print("BPM: ");
        Genotronex.println("HEART RATE IS  ");
        Genotronex.println(BPM);
        Serial.print(BPM);
        Serial.print("  ");
    } 
    else 
    {
        sendDataToSerial('B', BPM);
        sendDataToSerial('Q', IBI);
    }
}

void sendDataToSerial(char symbol, int data)
{
    Serial.print(symbol);
    Serial.println(data);
}

void ledFadeToBeat() 
{
    fadeRate -= 15;
    fadeRate = constrain(fadeRate, 0, 255);
    analogWrite(fadePin, fadeRate);
}

void arduinoSerialMonitorVisual(char symbol, int data) 
{
    const int sensorMin = 0;
    const int sensorMax = 1024;
    int sensorReading = data;
    int range = map(sensorReading, sensorMin, sensorMax, 0, 11);

/*
    switch (range) 
    {
        case 0:
            Serial.println("");
            break;
        case 1:
            Serial.println("---");
            break;
        case 2:
            Serial.println("------");
            break;
        case 3:
            Serial.println("---------");
            break;
        case 4:
            Serial.println("------------");
            break;
        case 5:
            Serial.println("--------------|-");
            break;
        case 6:
            Serial.println("--------------|---");
            break;
        case 7:
            Serial.println("--------------|-------");
            break;
        case 8:
            Serial.println("--------------|----------");
            break;
        case 9:
            Serial.println("--------------|----------------");
            break;
        case 10:
            Serial.println("--------------|-------------------");
            break;
        case 11:
            Serial.println("--------------|-----------------------");
            break;

    }
    */
}

void setupFunc()
{
    pinMode(blinkPin, OUTPUT);
    pinMode(fadePin, OUTPUT);
    Serial.begin(115200);
    interruptSetup();

    Genotronex.begin(9600);
    Genotronex.println("Bluetooth On please press 1 or 0 blink LED ..");
    pinMode(ledpin, OUTPUT);
}

void loopFunc()
{
  serialOutput();

    if (QS == true) 
    {
        digitalWrite(blinkPin, HIGH);
        fadeRate = 255;
        serialOutputWhenBeatHappens();
        QS = false;
    }
    else 
    {
        digitalWrite(blinkPin, LOW);
    }

    ledFadeToBeat();
    if (Genotronex.available()) 
    {
        BluetoothData = Genotronex.read();
        if (BluetoothData == '1') 
        {
            digitalWrite(ledpin, 1);
            Genotronex.println("LED  On D12 ON ! ");
        }
        if (BluetoothData == '0') 
        {
            digitalWrite(ledpin, 0);
            Genotronex.println("LED  On D12 Off ! ");
        }
    }
    delay(20);
}

#endif // HEART_RATE_BLUETOOTH

#ifdef GPS
void gpsSetup()
{
  Serial.begin(9600);
}

void gpsLoop()
{
  String lat = "49.8578";
  String lon = "24.0259";
  Serial.print("Latitude= "); 
  Serial.print(lat + random(100,999));
  Serial.print(" Longitude= "); 
  Serial.println(lon + random(100,999));  
}
#endif // GPS

void setup() 
{
#ifdef HEART_RATE_BLUETOOTH
   setupFunc();
#endif // HEART_RATE_BLUETOOTH

#ifdef MATRIX_ACCEL
   accelSetup();
#endif // MATRIX_ACCEL

#ifdef GPS
   gpsSetup();
#endif // GPS
}


void loop() 
{
#ifdef HEART_RATE_BLUETOOTH
   loopFunc();
#endif // HEART_RATE_BLUETOOTH

#ifdef MATRIX_ACCEL
   accelLoop();
#endif // MATRIX_ACCEL

#ifdef GPS
   gpsLoop();
#endif // GPS
}
